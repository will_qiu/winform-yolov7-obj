# Win-Form for inference onnx model of yolov7-object-detection 
This repository is primarily for performing inference with YOLOv7 object detection using a WinForm UI. We provide the ability to select models, photos, videos, or webcam input and perform inferences with different thresholds. It is user-friendly and easy to use.

## Getting Started

### Pre-requirements (Windows)

You can compile this repository using Visual Studio to obtain executable files.

- [Tutorial-visual-studio](https://visualstudio.microsoft.com/zh-hant/downloads/)


### EXE
We provide fully released files for download. After downloading, you can use them directly.

- URL: http://10.1.4.250/project1_data/BDM/Tools/winform-yolov7-obj.zip


<details>
    <summary> Demo
    </summary>
    <div align="center">
        <img width="90%" height="90%" src="./docs/demo.gif">
    </div>
</details>
