﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mask_detection
{
    class WinFormWaitingPanel : Form
    {
        public WinFormWaitingPanel(Form owner)
        {
            Owner = owner;

            // Size this form to cover the main form's client rectangle.
            Size = Owner.ClientRectangle.Size;
            FormBorderStyle = FormBorderStyle.None;
            UseWaitCursor = true;
            StartPosition = FormStartPosition.Manual;
            Location = PointToClient(
                Owner.PointToScreen(Owner.ClientRectangle.Location));

            // Add a label that says "Loading..."
            var label = new Label
            {
                Text = "Loading...",
                TextAlign = ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
            };
            Controls.Add(label);

            // Set the form color to see-through Blue
            BackColor = Color.LightBlue;
            Opacity = .5;

            var forceHandle = Handle;
            ShowDialog();
        }
    }
}
