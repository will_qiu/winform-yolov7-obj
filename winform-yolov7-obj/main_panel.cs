﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Diagnostics;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Threading;
using System.Web;
using System.Net.Sockets;

namespace yolov7_obj_detection
{
    public partial class main_panel : Form
    {
        private FilterInfoCollection videoDevices;
        public static string webcamIndex= "";

        public main_panel()
        {
            InitializeComponent();
            select_code selectCode = new select_code();
            selectCode.SelectCodeClosed += SelectCode_Closed;
            selectCode.ShowDialog();
        }

        private void SelectCode_Closed(object sender, EventArgs e)
        {
            ((select_code)sender).SelectCodeClosed -= SelectCode_Closed; // 取消事件订阅
            this.Show(); // 显示 main_panel
        }

        private void ClickSource(object sender, EventArgs e) {
            SourceList.Items.Clear();
            SourceList.Items.Add("Image");
            SourceList.Items.Add("Video");
            int count_index = 0;

            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            if (videoDevices.Count == 0)
            {
                MessageBox.Show("No video devices found.");
            }
            else
            {
                foreach (FilterInfo device in videoDevices)
                {
                    SourceList.Items.Add(device.Name + ":" + count_index.ToString());
                    count_index++;
                }
            }
        }

        // Choice action at source
        private void SourceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SourceList.SelectedItem != null)
            {
                string selectedText = SourceList.SelectedItem.ToString();
                //int selectedIndex = SourceList.SelectedIndex;
                //MessageBox.Show($"Selected Text: {selectedText}");
                if (selectedText != "Image" && selectedText != "Video")
                {
                    webcamIndex = selectedText.Split(':')[1];
                    //MessageBox.Show($"Selected Webcam index: {webcam_index}");
                }
                else
                {
                    string filter = "";
                    if (selectedText == "Image")
                    {
                        filter = "Image Files (*.bmp; *.jpg; *.jpeg; *.png)|*.bmp;*.jpg;*.jpeg;*.png";
                    }
                    else if (selectedText == "Video") 
                    {
                        filter = "Video Files (*.mp4; *.avi; *.mkv)|*.mp4;*.avi;*.mkv";
                    }

                    SelectFile(filter, SourceList);
                }
            }
        }

        private void ClickSelectModelFile(object sender, EventArgs e)
        {
            string filter = "Onnx Model Files (*.onnx)|*.onnx";
            SelectFile(filter, ModelList);

        }

        private void SelectFile(string filter, System.Windows.Forms.ComboBox comboBox)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = filter;
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //MessageBox.Show($"Selected FileName: {openFileDialog.FileName}");
                    if (!string.IsNullOrEmpty(openFileDialog.FileName)) {
                        comboBox.SelectedIndex = -1;
                        comboBox.Items.Clear();
                        comboBox.Items.Add(openFileDialog.FileName);
                        comboBox.SelectedIndex = 0;
                    }
                    //return openFileDialog.FileName;
                }
            }
            //return null;
        }

        private void RunBtn_Click(object sender, EventArgs e)
        {
            // Checking model path is not null
            if (string.IsNullOrEmpty(ModelList.Text)) { MessageBox.Show("Please select model file."); return; }

            // Checking source path is not null
            if (string.IsNullOrEmpty(SourceList.Text)) { MessageBox.Show("Please select source."); return; }
            if (SourceList.Text == "Image") { MessageBox.Show("Please select image file."); return; }
            if (SourceList.Text == "Video") { MessageBox.Show("Please select video file."); return; }
            string Sources = string.IsNullOrEmpty(webcamIndex) ? SourceList.Text : webcamIndex;

            // Checking threshold is or isnot between 0.0 and 1.0
            if (string.IsNullOrEmpty(Threshold.Text)) { MessageBox.Show("Threshold is null."); return; }
            float thresh = float.Parse(Threshold.Text);
            if (thresh > 1 && thresh < 0) { MessageBox.Show("The input value of Threshold should be between 0.0 and 1.0."); return; }

            // Running python inference
            //string pythonexepath = @".\infer-yolov7-obj\python\obj-yolov7-demo.exe";
            //string pythonfile = "";
            string pythonExePath = select_code.pythonPath;
            string pythonFile = @".\infer-yolov7-obj\python\obj-yolov7-demo.py";
            string modelPath = $"{pythonFile} -m {ModelList.Text}";
            string sourceValue = $"-i {Sources}";
            string thresholdValue = $"-t {Threshold.Text}";

            MessageBox.Show($"{pythonExePath} {modelPath} {sourceValue} {thresholdValue}");
            ExecutePythonExe(pythonExePath, modelPath, sourceValue, thresholdValue);
            System.Environment.Exit(0);
        }

        private void ExecutePythonExe(string pythonExePath, string modelPath, string sourceValue, string thresholdValue)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = pythonExePath,
                Arguments = $"{modelPath} {sourceValue} {thresholdValue}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            Process process = new Process
            {
                StartInfo = psi
            };

            this.Close();
            process.Start();

            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            process.WaitForExit();
            process.Close();

            Console.WriteLine("Python Output:");
            Console.WriteLine(output);

            if (!string.IsNullOrWhiteSpace(error))
            {
                Console.WriteLine("Python Error:");
                Console.WriteLine(error);
            }
        }
    }
}
