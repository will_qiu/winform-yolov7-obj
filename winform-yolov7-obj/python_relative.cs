﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Menu;

namespace yolov7_obj_detection
{
    class PythonRelative
    {
        install_prograss installPrograss = new install_prograss();

        static bool IsGitInstalled()
        {
            try
            {
                Process.Start("git", "--version");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static void CloneGitRepository(string repoUrl, string downloadPath)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = "git";
                process.StartInfo.Arguments = $"clone {repoUrl} {downloadPath}";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                process.WaitForExit();
            }
        }

        static void InstallGitWithChocolatey()
        {
            // 使用 Chocolatey 安装 Git
            string chocolateyCommand = "choco install git -y"; // -y 表示自动接受安装提示
            RunCommand(chocolateyCommand);
        }

        public void Checklibrary() {

            string libraryPath = "infer-yolov7-obj";
            string LogContext;
            int ProgressStep;
            bool isCreated = CheckDirExists(libraryPath);

            if (!isCreated)
            {
                installPrograss.Show();
                int TotalSteps = 2;

                string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string gitRepoUrl = "https://gitlab.com/will_qiu/yolov7-obj-infer.git";
                string downloadPath = Path.Combine(projectDirectory, libraryPath);

                LogContext = "Downloading Library...";
                ProgressStep = 1;
                ShowBarInfo(LogContext, TotalSteps, ProgressStep);

                if (IsGitInstalled())
                {
                    CloneGitRepository(gitRepoUrl, downloadPath);
                }
                else
                {
                    InstallGitWithChocolatey();
                    CloneGitRepository(gitRepoUrl, downloadPath);
                }

                LogContext = "Downloaded!";
                ProgressStep = 2;
                ShowBarInfo(LogContext, TotalSteps, ProgressStep);

            } 
            else {

                MessageBox.Show("The library is exists.");
            }
        }

        public string InstallPython()
        {
            string venvPythonExePath;

            // Checking Python env
            if (!CheckPythonInstalled())
            {
                installPrograss.Show();
                // Download install exe
                string LogContext = "Download python-3.10.2.";
                int totalsteps = 4;
                int progressstep = 1;
                ShowBarInfo(LogContext, totalsteps, progressstep);
                WgetPythonExe();
            }

            MessageBox.Show("Python is already installed.");
            venvPythonExePath = CreatePythonEnv();
            installPrograss.Close();
            return venvPythonExePath;
        }

        public string CreatePythonEnv() {

            string venvPath = "yolov7-env";
            string LogContext;
            int TotalStep = 3;
            int ProgressStep;
            bool isVenvCreated = CheckDirExists(venvPath);

            if (!isVenvCreated) {
                installPrograss.Show();

                LogContext = "Create new environment...";
                ProgressStep = 1;
                ShowBarInfo(LogContext, TotalStep, ProgressStep);
                RunCommand($"python -m venv {venvPath}");

                LogContext = "Activate venv and install requirements...";
                ProgressStep = 2;
                ShowBarInfo(LogContext, TotalStep, ProgressStep);
                RunCommand($"{venvPath}\\Scripts\\activate & pip install -r infer-yolov7-obj/python/env/requirements.txt");

                LogContext = "VenvPath is already exist...";
                ProgressStep = 3;
                ShowBarInfo(LogContext, TotalStep, ProgressStep);
            }
            string venvPythonExePath = $"{venvPath}\\Scripts\\python.exe";
            
            return venvPythonExePath;
        }

        static bool CheckDirExists(string Path)
        {
            return Directory.Exists(Path);
        }

        static bool CheckPythonInstalled()
        {
            Process process = new Process();
            process.StartInfo.FileName = "python";
            process.StartInfo.Arguments = "--version";
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;

            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return !string.IsNullOrEmpty(output);
        }

        public void WgetPythonExe() {
            string LogContext;
            int TotalSteps = 4;
            int ProgressStep;
            string pythonInstallerUrl = "https://www.python.org/ftp/python/3.10.2/python-3.10.2-amd64.exe"; 
            string pythonInstallerPath = "python_installer.exe"; 

            LogContext = "Downloading Python installer...";
            ProgressStep = 2;
            ShowBarInfo(LogContext, TotalSteps, ProgressStep);
            // Download install program
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadFile(pythonInstallerUrl, pythonInstallerPath);
            }

            LogContext = "Python installer downloaded...";
            ProgressStep = 3;
            ShowBarInfo(LogContext, TotalSteps, ProgressStep);
            RunCommand($"{pythonInstallerPath} /quiet InstallAllUsers=1 PrependPath=1");

            LogContext = "Python installation completed...";
            ProgressStep = 4;
            ShowBarInfo(LogContext, TotalSteps, ProgressStep);
        }

        public void ShowBarInfo(string LogContext, int TotalSteps, int ProgressStep) {

            Console.WriteLine(LogContext);
            string BarStep = $"{ProgressStep}/{TotalSteps}";
            installPrograss.LogWords.Text = LogContext;
            installPrograss.BarStep.Text = BarStep;
            int value = Convert.ToInt32(ProgressStep * 100 / TotalSteps);
            UpdateProgressBar(value);
            Thread.Sleep(500);
        }

        private void UpdateProgressBar(int value)
        {
            if (installPrograss.progressBar1.InvokeRequired)
            {
                installPrograss.progressBar1.Invoke(new Action<int>(UpdateProgressBar), value);
            }
            else
            {
                installPrograss.progressBar1.Value = value;
                installPrograss.Refresh();
                if (installPrograss.progressBar1.Value == 100) { MessageBox.Show("Installed."); }
            }
        }

        static void RunCommand(string command)
        {
            Process process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = "/c " + command;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;

            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            Console.WriteLine(output);
        }
    }
}
