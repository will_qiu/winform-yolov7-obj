﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yolov7_obj_detection
{
    public partial class select_code : Form
    {
        public static string pythonPath;
        public event EventHandler SelectCodeClosed;

        public select_code()
        {
            InitializeComponent();
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            SelectCodeClosed?.Invoke(this, EventArgs.Empty);
        }

        private void VersionSelectedIndexChanged(object sender, EventArgs e)
        {

            if (versionList.SelectedItem != null)
            {
                string selectedText = versionList.SelectedItem.ToString();
                MessageBox.Show($"Selected Version: {selectedText}");
                PythonRelative pythonInstaller = new PythonRelative();
                pythonInstaller.Checklibrary();

                if (selectedText == "Python")
                {
                    pythonPath = pythonInstaller.InstallPython();
                }
                else
                {
                    MessageBox.Show($"The Cpp version is still in development.");
                    Environment.Exit(0);
                }
            }

            this.Close();
        }
    }
}
